# ggOS ⁠· Windows 10 for Gamers 

Experience Windows 10 with hundreds of latency, performance, privacy, and quality of life tweaks.

The scripts included here are meant to help gamers manage and optimize Windows 10. Some scripts are my own, and some are the work of others. Each one has a specific, clearly defined purpose, and unless noted, will work independently of the others.

All scripts must be **Run as Administrator**.

## Disclaimer

These scripts have been tested on Windows 10 v20H2. They may or may not work on other versions of Windows. 

- This repository is not tech support.
- Use Google before asking questions.
- Never use a script you don't understand.
- Backup your computer before running a script.
- I am not responsible if you break your PC.

>THIS SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Credits

My thanks goes out to:

- [Bored](https://github.com/BoringBoredom)
- [Bunny](https://sites.google.com/view/winshit/)
- [Calypto](https://docs.google.com/document/d/1c2-lUJq74wuYK1WrA_bIvgb89dUN0sj8-hO3vqmrau4/view)
- [CatGamer](https://discord.io/FoxOS)
- [Danske](https://docs.google.com/document/d/18uPEXJC5LSto8x9X_GteSI58sfQLCfamDG1HNHJWrQU/view)
- [Fanta](https://discord.gg/PQZDM2KefF)
- [Felipe](https://github.com/Felipe8581/)
- [Melody](https://sites.google.com/view/melodystweaks/)
- [Riot](https://docs.google.com/document/d/1Bf155InFBbtztb3DykVD9njBZrduU4gc-M03LC0-TsE/edit)
- [SuperStrikE](https://github.com/SuperStrikEtweaks/Tweaking)
- [Timecard](https://github.com/djdallmann/GamingPCSetup/)
- [The Server.bat Team](https://discord.gg/nDRPTDctVf)
- [The Revision Team](https://www.revi.cc)

Almost everything I've learned about Windows optimization is because of their dedication and support.

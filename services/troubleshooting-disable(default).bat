:: DISABLE DIAGNOSTICS & TROUBLESHOOTING ON GGOS (DEFAULT)
:: https://gitlab.com/ggos/support

@echo off
setlocal ENABLEDELAYEDEXPANSION

:: CHECK FOR ADMIN PRIVILEGES
dism >nul 2>&1 || ( echo This script must be Run as Administrator. && pause && exit /b 1 )
devcon /? >nul 2>&1 || ( echo This script requires DEVCON in the system path. && pause && exit /b 1 )

:: DISABLE DIAGNOSTICS & TROUBLESHOOTING SERVICES
reg add "HKLM\SYSTEM\ControlSet001\Control\WMI\Autologger\DiagLog" /v "Start" /t REG_DWORD /d "0" /f >nul 2>&1
sc config DPS start=disabled >nul 2>&1
sc config TroubleshootingSvc start=disabled >nul 2>&1

echo "Diagnostics & Troubleshooting services have been disabled. Please restart your computer.
pause

/exit /b 0

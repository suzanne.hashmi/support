:: BACKUP STARTUP TYPE FOR ALL INSTALLED DRIVERS
:: https://gitlab.com/ggos/support

@echo off
setlocal ENABLEDELAYEDEXPANSION

:: CHECK FOR ADMIN PRIVILEGES
dism >nul 2>&1 || (echo This script must be Run as Administrator. && pause && exit /b 1)

if exist "%HOMEPATH%\Desktop\drivers-backup.bat" del /f /q "%HOMEPATH%\Desktop\drivers-backup.bat"
set "LOG=%HOMEPATH%\Desktop\drivers-backup.bat"

for /f "delims=," %%n in ('driverquery /FO CSV') do (
	for /f "tokens=2* skip=2" %%a in ('reg query "HKLM\SYSTEM\CurrentControlSet\Services\%%~n" /v "Start"') do ( 
		if "%%b"=="0x0" echo reg add "HKLM\SYSTEM\CurrentControlSet\Services\%%~n" /v "Start" /t REG_DWORD /d "0" >>%LOG%
		if "%%b"=="0x1" echo reg add "HKLM\SYSTEM\CurrentControlSet\Services\%%~n" /v "Start" /t REG_DWORD /d "1" >>%LOG%
		if "%%b"=="0x2" echo reg add "HKLM\SYSTEM\CurrentControlSet\Services\%%~n" /v "Start" /t REG_DWORD /d "2" >>%LOG%
		if "%%b"=="0x3" echo reg add "HKLM\SYSTEM\CurrentControlSet\Services\%%~n" /v "Start" /t REG_DWORD /d "3" >>%LOG%
		if "%%b"=="0x4" echo reg add "HKLM\SYSTEM\CurrentControlSet\Services\%%~n" /v "Start" /t REG_DWORD /d "4" >>%LOG%
	)
)
echo Your driver backup is complete. The file can be found on your desktop.
pause

exit /b 0
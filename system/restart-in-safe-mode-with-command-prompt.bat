:: RESTART IN SAFE MODE WITH COMMAND PROMPT
:: https://gitlab.com/ggos/support

@echo off
setlocal ENABLEDELAYEDEXPANSION

:: CHECK FOR ADMIN PRIVILEGES
dism >nul 2>&1 || (echo This script must be Run as Administrator. && pause && exit /b 1)

:: CHANGE BOOT SETTINGS
bcdedit /set safeboot minimal >nul 2>&1
bcdedit /set safebootalternateshell yes >nul 2>&1

:: RESTART
shutdown /r /d p:0:0 /c "Restarting in Safe Mode with Command Prompt..." >nul 2>&1

exit

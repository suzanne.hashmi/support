:: RESTART SYSTEM
:: https://gitlab.com/ggos/support

:: Usage: restart [ /c | /m | /n | /s ]

@echo off
setlocal ENABLEDELAYEDEXPANSION

:: CHECK FOR ADMIN PRIVILEGES
dism >nul 2>&1 || (echo This script must be Run as Administrator. && pause && exit /b 1)

bcdedit /deletevalue safebootalternateshell >nul 2>&1

if "%1"=="/?" goto :Help

if "%1"=="console" goto :Console
if "%1"=="/console" goto :Console
if "%1"=="command" goto :Console
if "%1"=="/command" goto :Console
if "%1"=="/c" goto :Console

if "%1"=="minimal" goto :Minimal
if "%1"=="/minimal" goto :Minimal
if "%1"=="/m" goto :Minimal

if "%1"=="network" goto :SafeMode
if "%1"=="/network" goto :SafeMode
if "%1"=="safemode" goto :SafeMode
if "%1"=="/safemode" goto :SafeMode
if "%1"=="/n" goto :SafeMode
if "%1"=="/s" goto :SafeMode

:: Restart in Normal Mode
bcdedit /deletevalue safebootalternateshell >nul 2>&1
bcdedit /deletevalue safeboot >nul 2>&1
shutdown /r /d p:0:0 /c "Restarting in Normal Mode..." >nul 2>&1
exit /b 0

:: Restart in Safe Mode with Command Prompt
:Console
bcdedit /set safeboot minimal >nul 2>&1
bcdedit /set safebootalternateshell yes >nul 2>&1
shutdown /r /d p:0:0 /c "Restarting in Safe Mode with Command Prompt..." >nul 2>&1
goto :eof

:: Restart in Safe Mode
:Minimal
bcdedit /deletevalue safebootalternateshell >nul 2>&1
bcdedit /set safeboot minimal >nul 2>&1
shutdown /r /d p:0:0 /c "Restarting in Safe Mode..." >nul 2>&1
goto :eof

:: Restart in Safe Mode with Networking
:SafeMode
bcdedit /deletevalue safebootalternateshell >nul 2>&1
bcdedit /set safeboot network >nul 2>&1
shutdown /r /d p:0:0 /c "Restarting in Safe Mode with Networking..." >nul 2>&1
goto :eof

:: Help
:Help
echo Restart the system.
echo.
echo USAGE:
echo     restart [option]
echo.
echo     Options:
echo         No args           Restart in normal mode.
echo.        /?                Display this help message.
echo         /c, /command      Restart in safe mode with command prompt.
echo         /m, /minimal      Restart in safe mode.
echo         /n, /network      Restart in safe mode with networking.
echo         /s, /safemode     Restart in safe mode with networking.
goto :eof

exit
